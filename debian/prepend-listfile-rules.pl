#!/usr/bin/perl -p

# Patch the login/ssh configs to check pam_listfile on admin nodes

# Note: tried using pam-auth-update, but no dice: we need to generally
# allow any kerberos user to authenticate for non-interactive
# services... but Debian's PAM framework only separates
# interactive/non-interactive session modules. It is possible to use
# pam_listfile as a session module, but this has the unfortunate side
# effect of allowing the account to authenticate &c before booting
# them during session setup.

# At least we can just shove this at the beginning of the file and be
# done with it.

BEGIN {
    print "#HCOOP BEGIN\n";
    print "# DO NOT MODIFY THIS BLOCK, IT WILL BE OVERWRITTEN UNCONDITIONALLY\n";
    print "account    requisite    pam_listfile.so item=user sense=allow file=/etc/login.restrict.hcoop onerr=succeed\n";
    print "auth       required pam_listfile.so item=user sense=allow file=/etc/login.restrict.hcoop  onerr=succeed\n";
    print "#HCOOP END\n";

}

# kill old block if one exists
if (/#HCOOP BEGIN/../#HCOOP END/) {
    $_ = '';
}
